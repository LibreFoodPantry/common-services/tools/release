#!/usr/bin/env bash

set -eo pipefail

if [[ "$DEBUG" == "true" ]] ; then
    set -x
fi

export MAJOR_MINOR_PATCH=
export MAJOR_MINOR=
export MAJOR=

main() {
    if docker manifest inspect "$CI_REGISTRY_IMAGE":"$CI_COMMIT_REF_SLUG" | jq -r '.manifests[].digest' &> /dev/null ; then
        multi_platform_release_image "$@"
    else
        standard_release_image "$@"
    fi
}

multi_platform_release_image() {
    parse-version "$1"
    log-into-gitlab

    AMENDMENTS=""
    while read -r sha ; do
        AMENDMENTS+=" --amend $CI_REGISTRY_IMAGE@$sha"
    done <<< "$(docker manifest inspect "$CI_REGISTRY_IMAGE":"$CI_COMMIT_REF_SLUG" | jq -r '.manifests[].digest')"

    echo "AMENDMENTS=$AMENDMENTS"

    create_and_push_manifest "$MAJOR" "$AMENDMENTS"
    create_and_push_manifest "$MAJOR_MINOR" "$AMENDMENTS"
    create_and_push_manifest "$MAJOR_MINOR_PATCH" "$AMENDMENTS"
    create_and_push_manifest latest "$AMENDMENTS"
}

create_and_push_manifest() {
    # shellcheck disable=SC2086 
    docker manifest create "$CI_REGISTRY_IMAGE":"$1" $2
    docker manifest push "$CI_REGISTRY_IMAGE":"$1"
}

standard_release_image() {
    parse-version "$1"
    log-into-gitlab
    if ! pull-branch-image ; then
        echo "No image tagged with main. Skipping release-image."
        exit 0
    fi
    push-release-images
}

parse-version() {
    MAJOR_MINOR_PATCH="${1}"
    MAJOR_MINOR="$(drop-last-component "$MAJOR_MINOR_PATCH")"
    MAJOR="$(drop-last-component "$MAJOR_MINOR")"
}

drop-last-component() {
    echo "${1%.*}"
}

log-into-gitlab() {
    docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
}

pull-branch-image() {
    docker pull "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
}

push-release-images() {
    tag-push "$MAJOR_MINOR_PATCH"
    tag-push "$MAJOR_MINOR"
    tag-push "$MAJOR"
    tag-push "latest"
}

tag-push() {
    VERSION="${1}"
    MAIN_IMAGE="$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
    RELEASE_IMAGE="$CI_REGISTRY_IMAGE:$VERSION"
    docker tag "$MAIN_IMAGE" "$RELEASE_IMAGE"
    docker push "$RELEASE_IMAGE"
}

main "$@"
