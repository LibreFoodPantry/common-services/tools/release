#!/usr/bin/env bash

echo "PREPARE_RELEASE_COMMAND=$PREPARE_RELEASE_COMMAND"

if [[ -z "$PREPARE_RELEASE_COMMAND" ]] ; then
    echo "WARNING: No PREPARE_RELEASE_COMMAND given. Skipping."
    exit 0
fi

if [[ ! -e "$PREPARE_RELEASE_COMMAND" ]] ; then
    echo "WARNING: PREPARE_RELEASE_COMMAND does not exist. Skipping"
    exit 0
fi

if [[ ! -x "$PREPARE_RELEASE_COMMAND" ]] ; then
    echo "ERROR: $PREPARE_RELEASE_COMMAND is not executable."
    exit 1
fi

mkdir -p artifacts/release
"$PREPARE_RELEASE_COMMAND" "$@"
