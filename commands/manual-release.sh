#!/usr/bin/env bash

commands/build.sh
docker tag release registry.gitlab.com/librefoodpantry/common-services/tools/release:latest
docker push registry.gitlab.com/librefoodpantry/common-services/tools/release:latest
docker tag release registry.gitlab.com/librefoodpantry/common-services/tools/release:main
docker push registry.gitlab.com/librefoodpantry/common-services/tools/release:main
