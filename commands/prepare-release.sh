#!/usr/bin/env bash

set -e

mkdir -p ./artifacts/release
echo "# Release ${1}" >> ./artifacts/release/release.yml
cat ./source/gitlab-ci/release.yml >> ./artifacts/release/release.yml
