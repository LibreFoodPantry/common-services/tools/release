#!/usr/bin/env bash
set -e

# Change to the root of the project.
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "$SCRIPT_DIR"
while [[ ! -d .git && "$(pwd)" != / ]] ; do cd .. ; done
if [[ ! -d .git ]] ; then
    echo "I must run from inside a git project."
    exit 1
fi

docker build --tag release ./source/container
