# Release

Provides a GitLab CI job that creates a new release when new commits
are added to the `main` branch.

* Version numbers follow SemVer and are computed based on Conventional
  Commit messages since the last release.
* The most recent commit on `main` is tagged with the new version number.
* The image in the project's container registry that has the `:main` suffix
  is tagged with the suffixes `:latest`, `:M.m.p`, `:M.m`, and `:M` where
  `M.m.p` is the SemVer `MAJOR.MINOR.PATH` version number.
* During the release process, it calls
  `$PREPARE_RELEASE_COMMAND` (by default `./commands/prepare-release.sh`)
  passing the new version number as the first command-line argument.
  The primary purpose of this command is to create the `./artifacts/release/`
  directory and place files in that directory. These files will be
  uploaded to the GitLab release entry.

## Usage

If you include Pipeline's template into your .gitlab-ci.yml file, this
project's template should already be included for you. If you would
prefer to use this independently of the Pipeline, you may include it
as follows.

```yaml
include:
    - remote: "https://gitlab.com/LibreFoodPantry/common-services/tools/release/-/raw/main/source/gitlab-ci/release.yml"
```

## Customization

See `release.yml` for variables that you can use to customize behavior.

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
